# Install certbot (snap required)
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo snap set certbot trust-plugin-with-root=ok
sudo snap install certbot-dns-cloudflare

echo -e "\n\n"
echo "Cerbot installed, please follow guides to properly configure cerbot:"
echo "Cloudflare Plugin: https://certbot-dns-cloudflare.readthedocs.io/en/stable/"
echo "CertBot: https://certbot.eff.org/lets-encrypt/ubuntufocal-nginx"
echo "Nginx tips: https://www.digitalocean.com/community/tools/nginx"
echo -e "\n\n"

