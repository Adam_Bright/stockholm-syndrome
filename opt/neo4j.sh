# Install Neo4j
_git_repo="https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master"

sudo wget -O - "${_git_repo}/neo4j-community-4.2.1-unix.tar.gz" | sudo tar -xzf -
sudo mv neo4j-community-4.2.1 /opt/neo4j-community-4.2.1
sudo adduser --disabled-password --gecos "" neo4j
sudo wget -O /opt/neo4j-community-4.2.1/neo4j.service "${_git_repo}/config/neo4j.service"
sudo chown -R neo4j:neo4j /opt/neo4j-community-4.2.1
ln -s /opt/neo4j-community-4.2.1/neo4j.service /etc/systemd/system/neo4j.service
sudo systemctl daemon-reload
sudo systemctl enable neo4j.service
sudo service neo4j start

