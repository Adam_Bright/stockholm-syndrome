# Install gitlab runner
sudo curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
sudo dpkg -i gitlab-runner_amd64.deb
sudo rm gitlab-runner_amd64.deb
# https://docs.gitlab.com/13.6/runner/install/linux-manually.html
# https://docs.gitlab.com/13.6/runner/register/index.html#linux
