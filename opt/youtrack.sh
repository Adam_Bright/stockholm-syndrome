# Install YouTrack
_git_repo="https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master"

sudo adduser --disabled-password --gecos "" youtrack
sudo wget -O /home/youtrack/youtrack.jar https://download.jetbrains.com/charisma/youtrack-2020.5.3123.jar
sudo chown youtrack:youtrack /home/youtrack/youtrack.jar
sudo wget -O /etc/systemd/system/youtrack.service "${_git_repo}/config/youtrack.service"
sudo systemctl daemon-reload
sudo systemctl enable youtrack.service
sudo service youtrack start

echo -e "\n\n"
echo "Successfully installed YouTrack"
echo "Please finish youtrack setup manually by opening :8090 port or whatever"
echo "Documentation: https://www.jetbrains.com/help/youtrack/standalone/Install-YouTrack-JAR-as-Service-Linux.html#upgrade-jar-as-service-linux"
echo -e "\n\n"