# Install ELK
# https://www.elastic.co/guide/en/elastic-stack-get-started/7.10/get-started-elastic-stack.html

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update

# ElasticSearch
sudo apt-get -y install elasticsearch
sudo service elasticsearch stop

sudo chown -R elasticsearch:elasticsearch /etc/elasticsearch

sudo sed -i "s/#JAVA_HOME.*/JAVA_HOME=\/lib\/jvm\/jdk-11.0.9/" /etc/default/elasticsearch
sudo sed -i "s/#network\.host.*/network\.host: 127\.0\.0\.1/" /etc/elasticsearch/elasticsearch.yml
sudo sed -i "s/#http\.port.*/http\.port: 9200/" /etc/elasticsearch/elasticsearch.yml
sudo sed -i "s/#discovery\.seed_hosts.*/discovery\.seed_hosts: \[\]/" /etc/elasticsearch/elasticsearch.yml

sudo sed -i "s/-Xms1g/-Xms4g/" /etc/elasticsearch/jvm.options
sudo sed -i "s/-Xmx1g/-Xmx4g/" /etc/elasticsearch/jvm.options

sudo service elasticsearch start
curl localhost:9200

# Kibana
sudo apt-get -y install kibana
sudo service kibana stop

sudo chown -R kibana:kibana /etc/kibana
sudo bash -c "echo \"JAVA_HOME=/usr/lib/jvm/jdk-11.0.9\" >> /etc/default/kibana"

sudo sed -i "s/#server\.port.*/server\.port: 5601/" /etc/kibana/kibana.yml
sudo sed -i "s/#server\.host.*/server\.host: \"127\.0\.0\.1\"/" /etc/kibana/kibana.yml

sudo service kibana start
curl http://localhost:5601

# LogStash
sudo apt-get -y install logstash
sudo service logstash stop

sudo chown -R logstash:logstash /etc/logstash
sudo bash -c "echo \"JAVA_HOME=/usr/lib/jvm/jdk-11.0.9\" >> /etc/default/logstash"

sudo service logstash start