# Install services
_git_repo="https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master"

read -rp "Setup nginx [yes]?" _nginx
[[ $_nginx == "" ]] && _nginx="yes"
[[ $_nginx == "${_nginx#[Yy]}" ]] && unset _nginx

read -rp "Setup certbot [yes]?" _certbot
[[ $_certbot == "" ]] && _certbot="yes"
[[ $_certbot == "${_certbot#[Yy]}" ]] && unset _certbot

read -rp "Setup redis [yes]?" _redis
[[ $_redis == "" ]] && _redis="yes"
[[ $_redis == "${_redis#[Yy]}" ]] && unset _redis

read -rp "Setup postgres [yes]?" _postgres
[[ $_postgres == "" ]] && _postgres="yes"
[[ $_postgres == "${_postgres#[Yy]}" ]] && unset _postgres

read -rp "Setup nats [yes]?" _nats
[[ $_nats == "" ]] && _nats="yes"
[[ $_nats == "${_nats#[Yy]}" ]] && unset _nats

read -rp "Setup youtrack [yes]?" _youtrack
[[ $_youtrack == "" ]] && _youtrack="yes"
[[ $_youtrack == "${_youtrack#[Yy]}" ]] && unset _youtrack

read -rp "Setup nexus [yes]?" _nexus
[[ $_nexus == "" ]] && _nexus="yes"
[[ $_nexus == "${_nexus#[Yy]}" ]] && unset _nexus

read -rp "Setup elastic [yes]?" _elastic
[[ $_elastic == "" ]] && _elastic="yes"
[[ $_elastic == "${_elastic#[Yy]}" ]] && unset _elastic

read -rp "Setup neo4j [yes]?" _neo4j
[[ $_neo4j == "" ]] && _neo4j="yes"
[[ $_neo4j == "${_neo4j#[Yy]}" ]] && unset _neo4j

read -rp "Setup gitlab runner [yes]?" _gitlab_runner
[[ $_gitlab_runner == "" ]] && _gitlab_runner="yes"
[[ $_gitlab_runner == "${_gitlab_runner#[Yy]}" ]] && unset _gitlab_runner

# Install Packages
[[ $_nginx ]] && wget -O - "${_git_repo}/opt/nginx.sh" | sudo bash
[[ $_certbot ]] && wget -O - "${_git_repo}/opt/certbot.sh" | sudo bash
[[ $_redis ]] && wget -O - "${_git_repo}/opt/redis.sh" | sudo bash
[[ $_postgres ]] && wget -O - "${_git_repo}/opt/postgres.sh" | sudo bash
[[ $_nats ]] && wget -O - "${_git_repo}/opt/nats.sh" | sudo bash
[[ $_youtrack ]] && wget -O - "${_git_repo}/opt/youtrack.sh" | sudo bash
[[ $_nexus ]] && wget -O - "${_git_repo}/opt/nexus.sh" | sudo bash
[[ $_elastic ]] && wget -O - "${_git_repo}/opt/elastic.sh" | sudo bash
[[ $_neo4j ]] && wget -O - "${_git_repo}/opt/neo4j.sh" | sudo bash
[[ $_gitlab_runner ]] && wget -O - "${_git_repo}/opt/gitlab-runner.sh" | sudo bash
