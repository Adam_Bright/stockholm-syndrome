echo "Welcome to Ubuntu 20.04 LTS setup"
echo "This script will automatically update system, install services and configure server for you"
echo "Before continuing please answer few questions"
echo "© cifrazia.com"

_git_repo="https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master"

[[ $USER == "root" ]] || sudo su root
_ip=$(dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | awk -F'"' '{ print $2}')

read -rp "Main user name [cifrazia]: " _main_user
[[ $_main_user == "" ]] && _main_user="cifrazia"
[[ $_main_user == "root" ]] && exit 1;
[[ $_main_user == "sudo" ]] && exit 1;

read -rsp "Password: " _pass
[[ $_pass == "" ]] && exit 1;
echo ""
read -rsp "Repeat: " _pass2
[[ $_pass == "$_pass2" ]] || exit 1;
unset _pass2
echo ""

# Update OS
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y autoremove

# Setting up locale
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
sudo update-locale "LANG=en_US.UTF-8"
sudo locale-gen en_US.UTF-8
sudo dpkg-reconfigure --frontend noninteractive locales

# System configuration
sudo bash -c "echo \"* soft nofile 65535\" >> /etc/security/limits.conf"
sudo bash -c "echo \"* hard nofile 65535\" >> /etc/security/limits.conf"
sudo bash -c "echo \"DefaultLimitNOFILE=65535\" >> /etc/systemd/user.conf"
ulimit -n 65535
sudo bash -c "echo 16777216 > /proc/sys/net/core/rmem_max"
sudo bash -c "echo 16777216 > /proc/sys/net/core/rmem_default"
sudo bash -c "echo 16777216 > /proc/sys/net/core/wmem_max"
sudo bash -c "echo 16777216 > /proc/sys/net/core/wmem_default"
sudo bash -c "echo \"4096 87380 16777216\" > /proc/sys/net/ipv4/tcp_rmem"
sudo bash -c "echo \"4096 87380 16777216\" > /proc/sys/net/ipv4/tcp_wmem"
sudo bash -c "echo \"1638400 1638400 1638400\" > /proc/sys/net/ipv4/tcp_mem"
sudo bash -c "echo 1 > /proc/sys/net/ipv4/tcp_sack"
sudo bash -c "echo 1 > /proc/sys/net/ipv4/tcp_dsack"
sudo bash -c "echo 0 > /proc/sys/net/ipv4/tcp_fack"
sudo bash -c "echo 0 > /proc/sys/net/ipv4/tcp_slow_start_after_idle"


# Install basic tools
sudo apt-get -y install tree \
	htop \
	net-tools \
	zip \
	git \
	nano \
	gcc \
	wget \
	curl \
	gcc \
	g++ \
	libssl-dev \
	make \
	build-essential \
	mlocate \
	checkinstall \
	openssh-server \
	ca-certificates\
	gnupg\
	lsb-release\
	apt-transport-https
sudo snap install core; sudo snap refresh core

cd || exit
ssh-keygen -b 4096 -C "Cifrazia $(uname -o -s -v)" -f cifrazia -m PEM -t rsa -N ""

# Install GoLang
sudo apt-get -y install golang

# Install JDK 11
[[ -d /usr/lib/jvm ]] || sudo mkdir /usr/lib/jvm
cd /usr/lib/jvm || exit
sudo wget https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master/jdk-11.0.9_linux-x64_bin.tar.gz
sudo tar -xzvf jdk-11.0.9_linux-x64_bin.tar.gz
sudo rm jdk-11.0.9_linux-x64_bin.tar.gz
cd jdk-11.0.9/bin/ || exit
sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk-11.0.9/bin/java 10001
sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk-11.0.9/bin/javac 10001
# sudo update-alternatives --install /usr/bin/javaws javaws /usr/lib/jvm/jdk-11.0.9/bin/javaws 10001
sudo bash -c "echo 'JAVA_HOME=/usr/lib/jvm/jdk-11.0.9' >> /etc/environment"
cd || exit

# Install JDK 8
[[ -d /usr/lib/jvm ]] || sudo mkdir /usr/lib/jvm
cd /usr/lib/jvm || exit
sudo wget https://gitlab.com/Aircodes-Adam/stockholm-syndrome/raw/master/jdk-8u212-linux-x64.tar.gz
sudo tar -xzvf jdk-8u212-linux-x64.tar.gz
sudo rm jdk-8u212-linux-x64.tar.gz
cd jdk1.8.0_212/bin/ || exit
sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk1.8.0_212/bin/java 10000
sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk1.8.0_212/bin/javac 10000
sudo update-alternatives --install /usr/bin/javaws javaws /usr/lib/jvm/jdk1.8.0_212/bin/javaws 10000
cd || exit

# Install Python 3.10
sudo apt-get -y install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev liblzma-dev
wget https://www.python.org/ftp/python/3.10.8/Python-3.10.8.tgz -O - | tar -xz
cd Python-3.10.8 || exit
./configure --enable-optimizations
make -j4
sudo make altinstall
cd || exit
sudo rm -rf Python-3.10.8
sudo python3.10 -m pip install --upgrade pip
sudo python3.10 -m pip install --upgrade setuptools wheel
sudo bash -c "echo \"alias py='python3'\" >> /etc/bash.bashrc"
sudo bash -c "echo \"alias pip='py -m pip'\" >> /etc/bash.bashrc"

# Install Python Package Manager [Poetry]
sudo python3.10 -m pip install poetry
python3.10 -m poetry config virtualenvs.create true
python3.10 -m poetry config virtualenvs.in-project true

# Install Node.JS
curl -sL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install Node.JS Package Manager [Yarn]
sudo npm install -g yarn

# Install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# Configure Journal rotation
sudo timedatectl set-timezone UTC
sudo timedatectl status
sudo journalctl --vacuum-size=256M
sudo journalctl --vacuum-time=1month

# Configure Access
sudo adduser --disabled-password --gecos "" $_main_user
sudo usermod -aG sudo $_main_user
echo -e "${_pass}\n${_pass}\n" | sudo passwd $_main_user
sudo bash -c "chown -R $_main_user:$_main_user /var/www"
sudo bash -c "chown -R $_main_user:$_main_user /home/$_main_user"

# Configure SSH access
echo "Don't forget to setup SSH-KEY only access to NOT root"
sudo wget "${_git_repo}/config/sshd.conf" -O /etc/ssh/sshd_config.d/sshd.conf
[[ -d /home/$_main_user/.ssh ]] || mkdir /home/$_main_user/.ssh
sudo bash -c "cat cifrazia.pub >> /home/$_main_user/.ssh/authorized_keys"
sudo service ssh restart
sudo wget "${_git_repo}/config/ufw-ssh.conf" -O /etc/ufw/applications.d/ufw-ssh.conf

# Fake SSH server
sudo apt-get install endlessh
sudo bash -c "echo 'net.ipv4.ip_unprivileged_port_start=0' > /etc/sysctl.d/50-unprivileged-ports.conf"
sudo sysctl --system
echo 'Port 22' > config
sudo mv config /etc/endlessh/config
sudo systemctl daemon-reload
sudo systemctl restart endlessh

# Configure NET filter
sudo apt-get -y install ufw
sudo ufw allow "OpenSSH"
sudo ufw allow "OurSSH"
yes | sudo ufw enable
sudo ufw status

# Install speedtest-cli
sudo apt-get -y install gnupg1 dirmngr
export INSTALL_KEY=379CE192D401AB61
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys $INSTALL_KEY
echo "deb https://ookla.bintray.com/debian generic main" | sudo tee  /etc/apt/sources.list.d/speedtest.list
sudo apt-get update
sudo apt-get -y install speedtest

# Final
go version
python3.10 --version
node -v
yarn -v
java -version
javac -version
docker run hello-world
docker rmi hello-world
docker-compose-version

echo -e "\n\n\nYour private SSH key for $_main_user \n#please verify that no empty lines are in the file\n\n"
sudo cat cifrazia
echo -e "\n\nYour SSH config:\n"
echo "Host cifrazia"
echo "    User $_main_user"
echo "    Port 300"
echo "    HostName $_ip"
echo "    PreferredAuthentications publickey"
echo "    IdentityFile ~/.ssh/cifrazia"
echo -e "\n\n\n\n"

echo "Reboot and check that everything is fired up"
